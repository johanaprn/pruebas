package ejercicios17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce dia 1");
		int dia = input.nextInt();
		System.out.println("Introduce mes 1");
		int mes = input.nextInt();
		System.out.println("Introduce a�o 1");
		int anno = input.nextInt();

		System.out.println("Introduce dia 2");
		int dia2 = input.nextInt();
		System.out.println("Introduce mes 2");
		int mes2 = input.nextInt();
		System.out.println("Introduce a�o 2");
		int anno2 = input.nextInt();

		// paso las dos fechas a numero de dias
		int diasFecha1 = dia + mes * 30 + anno * 360;
		int diasFecha2 = dia2 + mes2 * 30 + anno2 * 360;

		// calculo la diferencia
		int diferencia = diasFecha1 - diasFecha2;

		// si la primera fecha es anterior a la segunda
		// saldra una cantidad negativa
		// invierto el signo
		if (diferencia < 0) {
			diferencia = -diferencia;
		}

		System.out.println("La diferencia de dias es " + diferencia);

		input.close();

	}

}
