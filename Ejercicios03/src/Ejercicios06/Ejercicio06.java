package Ejercicios06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un caracter");
		char caracter1 = input.nextLine().charAt(0);

		if (caracter1 >= '0' && caracter1 <= '9') {
			System.out.println("Es una cifra");

		} else {
			System.out.println("No es una cifra ");

		}

		input.close();

	}

}
